var albumList;

axios.get("http://localhost:8000/album").then((response) => {
    albumList = response.data;
    albumList.forEach(element => {
        const tr = document.createElement("tr");
        tr.innerHTML = `<td>${element.Title}</td><td>${element.Artist}</td><td>${element.Rating}</td><td>${element.Price}</td>`;
        document.querySelector("tbody").appendChild(tr);
    });
})