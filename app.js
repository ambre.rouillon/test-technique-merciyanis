const express = require('express')
const fs = require("fs");
//let albums = require('./albums');
const http = require("http");
const app = express()
const host = 'localhost';
const port = 8000;

app.use(express.json())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header(
        "Access-Control-Allow-Headers",
        "Content-Type, Authorization, Content-Length, X-Requested-With"
    );

    app.options("/*", function (req, res, next) {
        res.sendStatus(200);
    })

    next();
})

const albums = JSON.parse(fs.readFileSync("albums.json"));


app.get('/api/album/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const album = albums.find(album => album.id === id)
    res.send(`Hello, album : ${album.Picture}`)
})

app.get('/album', (req, res) => {
    res.json(albums);
})

app.listen(8000);
console.log("serv running");
